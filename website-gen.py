import webbrowser
import argparse
import os
import datetime
from string import Template
# -documentation: https://docs.python.org/3/library/string.html#template-strings
import logging
import json
import urllib.parse
from helperfile import MySpecies, process_image, filter_func, Photographer, AllPhotographers

PUBLIC_DIR_STR = "public"
MAIN_TEMPLATE_FILE_STR = "main_template.html"
TARGET_INDEX_FILE_STR = "index.html"
TARGET_OSM_MAP_FILE_STR = "osm_map.html"
LOCATION_LINK_FILE_STR = "location-link.html"

SHARED_FOOTER_FILE_STR = "shared_footer.html"
SPECIES_PHOTO_CARD_FILE_STR = "species-photo-card.html"
MAIN_SPECIES_CARD_FILE_STR = "main-species-card.html"

SPECIES_TEMPLATE_FILE_STR = "species_template.html"
OSM_MAP_TEMPLATE_FILE_STR = "osm_map_template.html"

DESCRIPTION_FILE_NAME_STR = "description.txt"

parser = argparse.ArgumentParser()
parser.add_argument("--locally", action="store_true")
args = parser.parse_args()
running_locally_bool = args.locally

site_url_str = "https://sunyatazero.gitlab.io/nature-gallery/"
# -TODO: Grabbing this from an ini file
if running_locally_bool:
    site_url_str = "file:///home/sunyata/PycharmProjects/nature-gallery/public/"

tree_dir_list = [x.path for x in os.scandir(PUBLIC_DIR_STR) if x.is_dir()]
tree_dir_list = sorted(tree_dir_list, key=lambda x : x.lower())
print(tree_dir_list)
species_list = []
for tree_dir in tree_dir_list:
    if "experiment" in tree_dir and not running_locally_bool:
        continue  # -only showing experimental entries when running locally
    my_species = MySpecies(tree_dir)
    species_list.append(my_species)


ref_to_main_str = ".."
if running_locally_bool:
    ref_to_main_str = "../index.html"

with open(SHARED_FOOTER_FILE_STR, "r") as shared_footer_file:
    template = Template(shared_footer_file.read())
    completed_shared_footer_str = template.substitute(
        license="license_here"
    )

main_index_content_str = ''

"""
all_photographers = AllPhotographers()
pappa = Photographer("Torgny Dellsén", 1)
all_photographers.add_photographer(pappa, True)
# pappa.add_camera_model("")
jag = Photographer("Tord Dellsén", 2)
jag.add_camera_model("E2105")
jag.add_camera_model("Pixel")
all_photographers.add_photographer(jag)
"""

for iter_species in species_list:
    file_list = [x.path for x in os.scandir(iter_species.species_path_str) if x.is_file()]
    image_file_list = list(filter(filter_func, file_list))

    for image_file_path_str in image_file_list:
        my_image = process_image(image_file_path_str)
        iter_species.add_image(my_image)

    latlng_tuple_map_markers_list = []
    content_str = ''

    for iter_image in iter_species.get_image_list():
        # This tool can be used to compare result with: https://www.pic2map.com
        location_str = ""
        latitude_ft = iter_image.get_latitude()
        longitude_ft = iter_image.get_longitude()
        if latitude_ft != -1 and longitude_ft != -1:
            latlng_tuple_map_markers_list.append((latitude_ft, longitude_ft))

            with open(LOCATION_LINK_FILE_STR, "r") as location_link_file:
                template = Template(location_link_file.read())
                location_str = template.substitute(
                    latitude=latitude_ft,
                    longitude=longitude_ft
                )

            ########location_str = '<a target="_blank" rel="noopener noreferrer" class="card-details-osm" href="https://www.openstreetmap.org/#map=17/'+str(latitude_ft)+','+str(longitude_ft)+'"><img src="https://wiki.openstreetmap.org/w/images/7/79/Public-images-osm_logo.svg" width=20 width=20></a>'

        # https://wiki.openstreetmap.org/wiki/Logos

        month_str = "No date found"
        border_color_str = "#000000"
        date_str = ""
        time_photo_datetime: datetime = iter_image.time_photo_taken_datetime
        if iter_image.time_photo_taken_datetime is not None:
            month_str = time_photo_datetime.strftime("%B")
            date_str = time_photo_datetime.strftime("%Y-%m-%d")
            month_1_to_12_int = time_photo_datetime.month
            if month_1_to_12_int == 1:
                border_color_str = "#97ebe8"
            elif month_1_to_12_int == 2:
                border_color_str = "#97ebe8"
            elif month_1_to_12_int == 3:
                border_color_str = "#89fa7c"
            elif month_1_to_12_int == 4:
                border_color_str = "#89fa7c"
            elif month_1_to_12_int == 5:
                border_color_str = "#89fa7c"
            elif month_1_to_12_int == 6:
                border_color_str = "#fee753"
            elif month_1_to_12_int == 7:
                border_color_str = "#fee753"
            elif month_1_to_12_int == 8:
                border_color_str = "#fee753"
            elif month_1_to_12_int == 9:
                border_color_str = "#d5714c"
            elif month_1_to_12_int == 10:
                border_color_str = "#d5714c"
            elif month_1_to_12_int == 11:
                border_color_str = "#d5714c"
            elif month_1_to_12_int == 12:
                border_color_str = "#97ebe8"

        with open(SPECIES_PHOTO_CARD_FILE_STR, "r") as species_photo_card:
            template = Template(species_photo_card.read())
            content_str += template.substitute(
                image_basename=iter_image.get_image_basename(),
                thumbnail_basename=iter_image.get_thumbnail_basename(),
                month=month_str,
                date=date_str,
                location=location_str,
                border_color=border_color_str
            )

    search_html_str = urllib.parse.quote(iter_species.get_title())

    description_str = ""
    description_file_path_str = os.path.join(iter_species.species_path_str, DESCRIPTION_FILE_NAME_STR)
    if description_file_path_str in file_list:
        with open(description_file_path_str, "r") as description_file:
            description_str = description_file.read().strip()

    latlng_tuple_list_as_json_string = json.dumps(latlng_tuple_map_markers_list)
    logging.debug(latlng_tuple_list_as_json_string)

    osm_map_link_str = ""
    if len(latlng_tuple_map_markers_list) > 0:
        species_path_str = iter_species.species_path_str
        target_osm_map_path_str = os.path.join(species_path_str, TARGET_OSM_MAP_FILE_STR)
        with open(target_osm_map_path_str, "w") as osm_map_file, \
        open(OSM_MAP_TEMPLATE_FILE_STR, "r") as osm_map_template_file:
            template = Template(osm_map_template_file.read())
            complete_tree_index_file_str = template.substitute(
                latlng_list_as_string=latlng_tuple_list_as_json_string
            )
            osm_map_file.write(complete_tree_index_file_str)
        osm_map_link_str = '<li><a target="_blank" rel="noopener noreferrer" href="osm_map.html">Karta med alla hittade exemplar</a></li>'

    if iter_species.get_image_list():  # -at least one image in the directory
        # Main index
        link_str = iter_species.get_species_basename()
        if running_locally_bool:
            link_str = os.path.join(os.path.basename(iter_species.get_species_basename()), TARGET_INDEX_FILE_STR)
        ######main_index_content_str += '\n' + '<li><a href="' + link_str + '">' + iter_species.get_title() + '</a></li>'

        representing_image = iter_species.get_representing_image()
        thumb_base_str = representing_image.get_thumbnail_basename()
        thumb_path_str = os.path.join(iter_species.get_species_basename(), thumb_base_str)

        with open(MAIN_SPECIES_CARD_FILE_STR, "r") as main_species_card_file:
            template = Template(main_species_card_file.read())
            main_index_content_str += template.substitute(
                link=link_str,
                thumbnail_path=thumb_path_str,
                title=iter_species.get_title()
            )

        thumb_full_https_path_str = iter_species.get_thumbnail_https_full_path(site_url_str)
        species_full_https_path_str = iter_species.get_https_full_path(site_url_str)

        tree_index_file_str = os.path.join(iter_species.species_path_str, TARGET_INDEX_FILE_STR)
        with open(tree_index_file_str, "w") as tree_index_file, \
        open(SPECIES_TEMPLATE_FILE_STR, "r") as species_template_file:
            template = Template(species_template_file.read())
            complete_tree_index_file_str = template.substitute(
                ref_to_main=ref_to_main_str,
                title=iter_species.get_title(),
                species_full_https_path=species_full_https_path_str,
                species_thumb_full_https_path=thumb_full_https_path_str,
                description=description_str,
                search_term=search_html_str,
                content=content_str,
                osm_map_link=osm_map_link_str,
                footer=completed_shared_footer_str
            )
            tree_index_file.write(complete_tree_index_file_str)


with open(MAIN_TEMPLATE_FILE_STR, "r") as main_template_file:
    template = Template(main_template_file.read())
    complete_main_index_file_str = template.substitute(
        content=main_index_content_str,
        footer=completed_shared_footer_str
    )

with open(os.path.join(PUBLIC_DIR_STR, TARGET_INDEX_FILE_STR), "w") as main_index_file:
    main_index_file.write(complete_main_index_file_str)


if running_locally_bool:
    webbrowser.open(os.path.join(PUBLIC_DIR_STR, TARGET_INDEX_FILE_STR))
