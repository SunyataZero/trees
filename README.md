[![pipeline](https://gitlab.com/SunyataZero/nature-gallery/badges/master/pipeline.svg)](https://gitlab.com/SunyataZero/nature-gallery/-/pipelines)

# Nature Gallery

*Nature Gallery* is a static site generator for showing photos of different species of flowers, plants, trees, animals that the user has seen

Project status: Under development

Features:
* Easy to add new species and photos by creating new directories and copying the photos there
* Link to OpenStreetMap for each photo that has GPS data when uploaded
  * For a species you can see a map with markers for each of the photos which has GPS data
* Date is extracted from the photos and shown in the gallery
   * The month is used to set the color of the frame of the photo (light green for april, for example)
* Can also be used locally or online
  * If using GitLab: The website is automatically generated when adding a new image

### Demo

**https://sunyatazero.gitlab.io/nature-gallery**

### Usage

Assumption: You have cloned the repo or downloaded the code

It's possible to run locally (for testing purposes) or on a server

#### To add a new species..

..add a new directory in the "public" directory

#### To add a new photo for a species..

..add the photo in the directory with the name of the species

#### To add a description for a species..

..add a text file with the name "description.txt" inside the directory of the species

#### To generate the new site..

**..run website-gen.py**

### License

GPLv3
