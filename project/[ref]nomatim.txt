url_for_request_str = "https://nominatim.openstreetmap.org/reverse?format=json&lat="+str(latitude_ft)+"&lon="+str(longitude_ft)+"&zoom=18&addressdetails=1"
urllib_request = urllib.request.Request(url_for_request_str, headers={'User-Agent': 'Mozilla/5.0'})
my_url = urllib.request.urlopen(urllib_request)
# Usage policy: https://operations.osmfoundation.org/policies/nominatim/
# https://nominatim.openstreetmap.org/reverse?format=json&lat='+str(latitude_ft)+'&lon='+str(longitude_ft)+'&zoom=18&addressdetails=1
# nominatim_display_name_json_data = json.loads(url.read().decode())
# print(nominatim_display_name_json_data.display_name)

# https://nominatim.openstreetmap.org/reverse?format=json&lat=57.6582222&lon=12.0370167&zoom=18&addressdetails=1
# -formats: json, xml
