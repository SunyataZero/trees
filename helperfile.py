import datetime
import logging
import os.path
import typing
import urllib.parse
import PIL.ExifTags
import PIL.Image

DATETIME_ORIGINAL_EXIF_TAG_NAME_STR = "DateTimeOriginal"
MODEL_EXIF_TAG_NAME_STR = "Model"
PY_DATETIME_FORMAT_STR = "%Y:%m:%d %H:%M:%S"

SIZE_TE = (270, 270)
RGB_STR = 'RGB'
JPEG_FORMAT_STR = "JPEG"
THUMBNAIL_FILE_ENDING_STR = "_thumbnail.jpg"


class MyImage:
    def __init__(self, i_image_path: str, i_time_photo_taken: datetime, i_lat_lng: typing.Tuple, i_camera_model: str):
        self.image_path_str = i_image_path
        self.time_photo_taken_datetime = i_time_photo_taken
        self.lat_lng_tuple = i_lat_lng
        self.camera_model_str = i_camera_model

    def get_image_basename(self) -> str:
        image_basename_str = os.path.basename(self.image_path_str)
        return image_basename_str

    def get_thumbnail_basename(self) -> str:
        image_basename_str = self.get_image_basename()
        thumbnail_basename_str = os.path.splitext(image_basename_str)[0] + THUMBNAIL_FILE_ENDING_STR
        return thumbnail_basename_str

    def get_latitude(self) -> float:
        latitude_ft = self.lat_lng_tuple[0]
        return latitude_ft

    def get_longitude(self) -> float:
        longitude_ft = self.lat_lng_tuple[1]
        return longitude_ft

    """
    def get_photographer(self) -> str:
        if i_camera_model
    """


class Photographer:
    def __init__(self, i_name: str, i_prio: int):
        self.name_str = i_name
        self.prio_int = i_prio
        self.camera_model_list = []
        # Default license
        # Website

    """
    def set_name(self, i_name: str):
        self.name_str = i_name

    def set_prio(self, i_prio: int):
        self.prio_int = i_prio
    """

    def add_camera_model(self, i_camera_model: str):
        self.camera_model_list.append(i_camera_model)

    def has_camera_model(self, i_camera_model: str) -> bool:
        if i_camera_model in self.camera_model_list:
            return True
        else:
            return False


class AllPhotographers:
    def __init__(self):
        self.photographer_list = []

    def add_photographer(self, i_photographer: Photographer, i_is_default: bool=False):
        self.photographer_list.append((i_photographer, i_is_default))

    def get_photographer_from_camera_model(self, i_camera_model: str) -> Photographer:
        for (p, _) in self.photographer_list:
            if p.has_camera_model(i_camera_model):
                return p
        # If no camera model has been found, look for if one of the photographers is default
        for (p, is_default) in self.photographer_list:
            if is_default:
                return p
        return None


class MySpecies:
    def __init__(self, i_species_path: str):
        self.species_path_str = i_species_path
        self.image_list = []

    def get_species_basename(self):
        return os.path.basename(self.species_path_str)

    def get_title(self) -> str:
        title_str = self.get_species_basename()
        title_str = title_str.replace('_', ' ')
        title_str = title_str.title()
        return title_str

    def get_representing_image(self):
        if len(self.image_list) > 0:
            return self.get_image_list()[0]
        else:
            return None

    def add_image(self, i_image: MyImage) -> None:
        self.image_list.append(i_image)

    """
    def get_image_list_for_photographer(self, i_photographer: str) -> [MyImage]:
        image_list = sorted(self.image_list, key=lambda x: x.time_photo_taken_datetime, reverse=True)
        image_list = filter(image_list, photographer_filter_func)
        return image_list
    """

    def get_image_list(self) -> [MyImage]:
        image_list = sorted(self.image_list, key=lambda x: x.time_photo_taken_datetime, reverse=True)
        return image_list

    def get_thumbnail_https_full_path(self, i_site_url: str) -> str:
        representing_image = self.get_representing_image()
        thumb_base_str = representing_image.get_thumbnail_basename()
        thumb_path_str = os.path.join(self.get_species_basename(), thumb_base_str)
        thumbnail_https_full_path_str = urllib.parse.urljoin(i_site_url, thumb_path_str)
        # Example: https://sunyatazero.gitlab.io/nature-gallery/backtrav/IMG_20190414_163047_thumbnail.jpg
        return thumbnail_https_full_path_str

    def get_https_full_path(self, i_site_url: str) -> str:
        species_https_full_path_str = urllib.parse.urljoin(i_site_url, self.get_species_basename())
        return species_https_full_path_str


"""
def photographer_filter_func(i_image: MyImage, ):
    i_image.camera_model_str
    all_photographers.get_photographer_from_camera_model()
    for photographer in all_photographers:
        photographer..has_camera_model(i_image.camera_model_str)
    ret_val_bool = is_image_file_bool and not file_is_thumbnail_bool
    return ret_val_bool
"""


def get_decimal_degrees(i_pil_gps_info_tuple: tuple) -> float:
    """
    This function is called separately for latitude (west-east) and longitude (south-north)
    """
    # formatted_str_list = i_ifdtag.split(", ")
    # Please note that the space is important since the (arc-) seconds also may have a comma in them

    degrees_int = i_pil_gps_info_tuple[0]
    minutes_int = i_pil_gps_info_tuple[1]
    seconds_float = i_pil_gps_info_tuple[2]

    """
    degrees_int = i_pil_gps_info_tuple[0][0] / i_pil_gps_info_tuple[0][1]
    minutes_int = i_pil_gps_info_tuple[1][0] / i_pil_gps_info_tuple[1][1]
    seconds_float = i_pil_gps_info_tuple[2][0] / i_pil_gps_info_tuple[2][1]
    """

    result_ft = float(degrees_int + minutes_int / 60 + seconds_float / 3600)

    return result_ft


def get_datetime_image_taken(i_image_pi: PIL.Image) -> datetime:
    # Please note that usually (always?) the time we get from the carmera is in the UTC time zone:
    # https://photo.stackexchange.com/questions/82166/is-it-possible-to-get-the-time-a-photo-was-taken-timezone-aware
    # So we need to convert the time that we get
    ret_datetime_qdt = None
    datetime_original_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == DATETIME_ORIGINAL_EXIF_TAG_NAME_STR:
            datetime_original_tag_key_str = tag_key_str
            break
    if datetime_original_tag_key_str == "":
        logging.warning("get_datetime_image_taken - exif tag not found")
    else:
        try:
            exif_data_dict = dict(i_image_pi._getexif().items())
            # -Good to be aware that _getexif() is an experimental function:
            #  https://stackoverflow.com/a/48428533/2525237
            datetime_exif_string = exif_data_dict[datetime_original_tag_key_str]
            logging.debug("datetime_exif_string = " + datetime_exif_string)
            from_camera_qdt = datetime.datetime.strptime(datetime_exif_string, PY_DATETIME_FORMAT_STR)
            ######from_camera_qdt.setTimeSpec(QtCore.Qt.UTC)
            ######ret_datetime_qdt = from_camera_qdt.toLocalTime()
            ret_datetime_qdt = from_camera_qdt
        except AttributeError:
            # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
            #  so instead we use this exception handling
            logging.warning("get_datetime_image_taken - Image doesn't have exif data. This may be because it has already been processed by an application")
        except KeyError:
            logging.warning("KeyError: Unknown why this happens but only seems to happen non-locally (i.e. on gitlab)")

    return ret_datetime_qdt


def get_camera_model(i_image_pi: PIL.Image) -> str:
    # Please note that usually (always?) the time we get from the carmera is in the UTC time zone:
    # https://photo.stackexchange.com/questions/82166/is-it-possible-to-get-the-time-a-photo-was-taken-timezone-aware
    # So we need to convert the time that we get
    model_exif_string = ""
    model_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == MODEL_EXIF_TAG_NAME_STR:
            model_tag_key_str = tag_key_str
            break
    if model_tag_key_str == "":
        logging.warning("get_camera_model - exif tag not found")
    else:
        try:
            exif_data_dict = dict(i_image_pi._getexif().items())
            # -Good to be aware that _getexif() is an experimental function:
            #  https://stackoverflow.com/a/48428533/2525237
            model_exif_string = exif_data_dict[model_tag_key_str]
            logging.debug("model_exif_string = " + model_exif_string)
        except AttributeError:
            # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
            #  so instead we use this exception handling
            logging.warning("get_camera_model - Image doesn't have exif data. This may be because it has already been processed by an application")
        except KeyError:
            logging.warning("KeyError: Unknown why this happens but only seems to happen non-locally (i.e. on gitlab)")

    return model_exif_string


def get_lat_long_for_image(i_image_pi: PIL.Image) -> (float, float):
    # GOOD STUFF:
    # Reference: https://stackoverflow.com/questions/19804768/interpreting-gps-info-of-exif-data-from-photo-in-python
    # (So there are three different solutions that we can use)
    # Using PIL/Pillow: https://gist.github.com/erans/983821

    latitude_ft = -1
    longitude_ft = -1

    ###ret_datetime_qdt = None
    gps_info_tag_key_str = ""
    for tag_key_str in PIL.ExifTags.TAGS.keys():
        if PIL.ExifTags.TAGS[tag_key_str] == "GPSInfo":
            gps_info_tag_key_str = tag_key_str
            break
    if gps_info_tag_key_str == "":
        logging.warning("get_lat_long_for_image - exif tag not found")
    else:
        try:
            exif_data_dict = dict(i_image_pi._getexif().items())
            # -Good to be aware that _getexif() is an experimental function:
            #  https://stackoverflow.com/a/48428533/2525237
            for exif_key_str, value in exif_data_dict.items():
                if exif_key_str == gps_info_tag_key_str:
                    gps_dict = value
                    for tag_key_str in PIL.ExifTags.GPSTAGS.keys():
                        if PIL.ExifTags.GPSTAGS[tag_key_str] == "GPSLatitude":
                            gps_lat_tag_key_str = tag_key_str
                            gps_dict_lat_te = gps_dict[gps_lat_tag_key_str]
                            latitude_ft = get_decimal_degrees(gps_dict_lat_te)
                        elif PIL.ExifTags.GPSTAGS[tag_key_str] == "GPSLongitude":
                            gps_lng_tag_key_str = tag_key_str
                            gps_dict_lng_te = gps_dict[gps_lng_tag_key_str]
                            longitude_ft = get_decimal_degrees(gps_dict_lng_te)
            #####gps_info_exif_string = exif_data_dict[gps_info_tag_key_str]
            #####logging.debug("gps_latitude_exif_string = " + gps_info_exif_string)
            # from_camera_qdt = datetime.datetime.strptime(gps_latitude_exif_string, PY_DATETIME_FORMAT_STR)
            ######from_camera_qdt.setTimeSpec(QtCore.Qt.UTC)
            ######ret_datetime_qdt = from_camera_qdt.toLocalTime()
            # ret_datetime_qdt = from_camera_qdt
        except AttributeError:
            # -A strange problem: If we use hasattr(i_image_pi, "_getexif") this will return True,
            #  so instead we use this exception handling
            logging.warning("get_datetime_image_taken - Image doesn't have exif data. This may be because it has already been processed by an application")

    return (latitude_ft, longitude_ft)


def process_image(i_file_path: str) -> MyImage:
    image_pi: PIL.Image = PIL.Image.open(i_file_path)

    # Time when the photo was taken
    time_photo_taken_pdt = get_datetime_image_taken(image_pi)
    # -important that this is done before rotating since rotating removes exif data

    camera_model_str = get_camera_model(image_pi)

    lat_lng_tuple = get_lat_long_for_image(image_pi)

    my_image = MyImage(i_file_path, time_photo_taken_pdt, lat_lng_tuple, camera_model_str)

    """
    # Rotating
    rotation_degrees_int = get_rotation_degrees(image_pi)
    if rotation_degrees_int != 0:
        image_pi = image_pi.rotate(rotation_degrees_int, expand=True)
        # -Warning: Rotating removes exif data (unknown why)
    """

    if image_pi.mode != RGB_STR:
        image_pi = image_pi.convert(RGB_STR)
    #  How to check is described in this answer: https://stackoverflow.com/a/43259180/2525237
    image_thumbnail_name_str = os.path.splitext(i_file_path)[0] + THUMBNAIL_FILE_ENDING_STR
    if not os.path.isfile(image_thumbnail_name_str):
        image_pi.thumbnail(SIZE_TE, PIL.Image.ANTIALIAS)
        image_pi.save(image_thumbnail_name_str, format=JPEG_FORMAT_STR)
        # -Please note that exif metadata is removed!
        #  If we want to keep exif metadata: https://stackoverflow.com/q/17042602/2525237

    """
    # print(PIL.ExifTags.GPSTAGS)
    # print(type(PIL.ExifTags.GPSTAGS))
    print("GPS keys:")
    for gps_key, gps_value in PIL.ExifTags.GPSTAGS.items():
        print("gps_key = " + str(gps_key))
        print("gps_value = " + str(gps_value))
    """

    return my_image


def filter_func(i_file_name: str):
    is_image_file_bool = i_file_name.lower().endswith((".png", ".jpg", ".jpeg", ".PNG", ".JPG", ".JPEG"))
    file_is_thumbnail_bool = i_file_name.endswith(THUMBNAIL_FILE_ENDING_STR)
    ret_val_bool = is_image_file_bool and not file_is_thumbnail_bool
    return ret_val_bool
